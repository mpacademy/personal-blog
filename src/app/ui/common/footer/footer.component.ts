import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  ViewChild,
} from '@angular/core';
import {
  distinctUntilChanged,
  map,
  Observable,
  ReplaySubject,
  scan,
  startWith,
  switchMap,
  takeUntil,
  takeWhile,
} from 'rxjs';
import { TRANSITION_REVEAL } from '../../animations/transitions/transitions.constants';
import { UiUtilsView } from '../../utils/views.utils';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  animations: [TRANSITION_REVEAL],
})
export class FooterComponent implements AfterViewInit {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  _mThreshold = 0.4;
  mOnceAnimated = false;
  _mTriggerAnim? = 'false';

  @ViewChild('animRefView') vAnimRefView?: ElementRef<HTMLElement>;

  constructor(
    private _ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private scroll: ScrollDispatcher,
    private viewPortRuler: ViewportRuler
  ) {}

  ngAfterViewInit(): void {
    this.setupAnimation();
  }

  public setupAnimation() {
    if (!this.vAnimRefView) return;

    this.scroll
      .ancestorScrolled(this.vAnimRefView, 100)
      .pipe(
        // Makes sure to dispose on destroy
        takeUntil(this.destroyed$),
        startWith(0),
        map(() => {
          if (this.vAnimRefView != null) {
            const visibility = UiUtilsView.getVisibility(
              this.vAnimRefView,
              this.viewPortRuler
            );
            return visibility;
          }
          return 0;
        }),
        scan<number, boolean>(
          (acc: number | boolean, val: number) =>
            val >= this._mThreshold || (acc ? val > 0 : false)
        ),
        // Distinct the resulting triggers
        distinctUntilChanged(),
        takeWhile(trigger => {
          return !trigger || !this.mOnceAnimated;
        }, true),
        switchMap(
          trigger =>
            new Observable<number | boolean>(observer =>
              this._ngZone.run(() => observer.next(trigger))
            )
        )
      )
      .subscribe(val => {
        if (this.mOnceAnimated) {
          return;
        }

        if (val) {
          this.mOnceAnimated = true;
          this._mTriggerAnim = 'true';
          this.cdr.detectChanges();
        }
      });
  }
}
