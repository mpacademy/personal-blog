import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogModelProgress } from '../model';

@Component({
  selector: 'app-dialog-progress',
  templateUrl: './dialog-progress.component.html',
  styleUrls: ['./dialog-progress.component.scss'],
})
export class DialogProgressComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogProgressComponent>,
    @Inject(MAT_DIALOG_DATA) public _mData: DialogModelProgress
  ) {}

  onSubmit() {
    this.dialogRef.close(this._mData);
  }
}
