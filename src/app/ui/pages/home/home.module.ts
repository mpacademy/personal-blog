import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../../@shared/shared.module';
import { HomeTopComponent } from './home-top/home-top.component';
import { HomeAboutComponent } from './home-about/home-about.component';
import { FooterModule } from '../../common/footer/footer.module';
import { HomeShowcasesComponent } from './home-showcases/home-showcases.component';
import { HomeExpertiseComponent } from './home-expertise/home-expertise.component';
import { ExpertiseItemComponent } from './home-expertise/expertise-item/expertise-item.component';
import { HomeContactComponent } from './home-contact/home-contact.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePlatformsComponent } from './home-platforms/home-platforms.component';
import { AppItemComponent } from './home-platforms/app-item/app-item.component';
import { IconsComponent } from './home-platforms/icons/icons.component';
import { SuccessModule } from '../../common/success/success.module';
import { ProgressModule } from '../../common/progress/progress.module';

import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';
import { MyAppsComponent } from './home-platforms/my-apps/my-apps.component';
import { MyappItemComponent } from './home-platforms/myapp-item/myapp-item.component';
import { ScrollModule } from '../../../core/scroll/scroll.module';

@NgModule({
  declarations: [
    HomeComponent,
    HomeTopComponent,
    HomeAboutComponent,
    HomePlatformsComponent,
    HomeExpertiseComponent,
    ExpertiseItemComponent,
    HomeContactComponent,
    HomeShowcasesComponent,
    AppItemComponent,
    IconsComponent,
    MyAppsComponent,
    MyappItemComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MatButtonModule,
    FooterModule,
    ReactiveFormsModule,
    FormsModule,

    MatFormFieldModule,
    MatInputModule,
    ProgressModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    SuccessModule,
    ScrollModule,
  ],
})
export class HomeModule {}
