import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-expertise-item',
  templateUrl: './expertise-item.component.html',
  styleUrls: ['./expertise-item.component.scss'],
})
export class ExpertiseItemComponent {
  @Input() set jobType(data: string) {
    if (data) {
      this._mJobType = data;
    }
  }

  @Input() set parts(data: string[]) {
    if (data && data.length > 0) {
      this._mParts = data.join(' | ');
    }
  }

  @Input() set isColored(data: boolean) {
    if (data) {
      this._mIsColored = data;
    }
  }
  _mParts = '';
  _mJobType = '';
  _mIsColored = false;

  constructor() {}
}
