import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  distinctUntilChanged,
  map,
  Observable,
  ReplaySubject,
  scan,
  startWith,
  switchMap,
  takeUntil,
  takeWhile,
} from 'rxjs';
import {
  TRANSITION_IMAGE_SCALE,
  TRANSITION_TEXT,
} from 'src/app/ui/animations/transitions/transitions.constants';
import { UiUtilsView } from 'src/app/ui/utils/views.utils';

@Component({
  selector: 'app-home-expertise',
  templateUrl: './home-expertise.component.html',
  styleUrls: ['./home-expertise.component.scss'],
  animations: [TRANSITION_TEXT, TRANSITION_IMAGE_SCALE],
})
export class HomeExpertiseComponent implements AfterViewInit, OnDestroy {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  mOnceAnimated = false;

  _mTriggerAnim? = 'false';

  _mTriggerImage? = 'false';

  _mThreshold = 0.2;

  @ViewChild('animRefView') vAnimRefView?: ElementRef<HTMLElement>;

  constructor(
    public el: ElementRef,
    private _ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private scroll: ScrollDispatcher,
    private viewPortRuler: ViewportRuler
  ) {}

  ngAfterViewInit(): void {
    this.setupAnimation();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  public setupAnimation() {
    if (!this.vAnimRefView) return;

    this.scroll
      .ancestorScrolled(this.vAnimRefView, 100)
      .pipe(
        // Makes sure to dispose on destroy
        takeUntil(this.destroyed$),
        startWith(0),
        map(() => {
          if (this.vAnimRefView != null) {
            const visibility = UiUtilsView.getVisibility(
              this.vAnimRefView,
              this.viewPortRuler
            );
            return visibility;
          }
          return 0;
        }),
        scan<number, boolean>(
          (acc: number | boolean, val: number) =>
            val >= this._mThreshold || (acc ? val > 0 : false)
        ),
        // Distinct the resulting triggers
        distinctUntilChanged(),
        takeWhile(trigger => {
          return !trigger || !this.mOnceAnimated;
        }, true),
        switchMap(
          trigger =>
            new Observable<number | boolean>(observer =>
              this._ngZone.run(() => observer.next(trigger))
            )
        )
      )
      .subscribe(val => {
        if (this.mOnceAnimated) {
          return;
        }

        if (val) {
          this.mOnceAnimated = true;
          this._mTriggerAnim = 'true';
          this.cdr.detectChanges();
        }
      });
  }

  _mTools = [
    // design
    {
      id: '5131',
      name: 'Figma',
      logo: 'assets/img/tools/figma.svg',
      link: 'https://www.figma.com/',
      tab: 'design',
    },
    {
      id: '5132',
      name: 'Adobe XD',
      logo: 'assets/img/tools/xd.png',
      link: 'https://www.adobe.com/products/xd.html',
      tab: 'design',
    },

    // cross
    {
      id: '4101',
      name: 'Flutter',
      logo: 'assets/img/tools/flutter_logo.svg',
      link: 'https://flutter.dev/',
      tab: 'cross',
    },
    {
      id: '4102',
      name: 'React Native',
      logo: 'assets/img/tools/react-native.svg',
      link: 'https://reactnative.dev/',
      tab: 'cross',
      color: '#282C34'
    },

    // web
    {
      id: '8101',
      name: 'TypeScript',
      logo: 'assets/img/tools/typescript.png',
      link: 'https://www.typescriptlang.org/',
      tab: 'web',
      color: '#e09b41',
    },
    {
      id: '8102',
      name: 'React',
      logo: 'assets/img/tools/react.svg',
      link: 'https://react.dev/',
      tab: 'web',
      color: '#087EA4',
    },
    {
      id: '8103',
      name: 'Redux',
      logo: 'assets/img/tools/redux.svg',
      link: 'https://redux.js.org/',
      tab: 'web',
    },
    {
      id: '8104',
      name: 'Vue',
      logo: 'assets/img/tools/vue.svg',
      link: 'https://vuejs.org/',
      tab: 'web',
    },
    {
      id: '8105',
      name: 'Highcharts',
      logo: 'assets/img/tools/highchart-logo.png',
      link: 'https://www.highcharts.com/',
      tab: 'web',
    },
    {
      id: '8106',
      name: 'Angular',
      logo: 'assets/img/tools/angular.png',
      link: 'https://angular.io/',
      tab: 'web',
      color: '#FF4369',
    },
    {
      id: '8107',
      name: 'RxJS',
      logo: 'assets/img/tools/rxjs.svg',
      link: 'https://rxjs.dev/',
      tab: 'web',
      color: '#ce6b9d',
    },
    {
      id: '8108',
      name: 'Sass',
      logo: 'assets/img/tools/sass-logo.svg',
      link: 'https://sass-lang.com/',
      tab: 'web',
    },
    {
      id: '8109',
      name: 'NgRx',
      logo: 'assets/img/tools/ngrx.svg',
      link: 'https://ngrx.io/',
      tab: 'web',
    },
    {
      id: '8110',
      name: 'Material Design',
      logo: 'assets/img/tools/material.svg',
      link: 'https://m3.material.io/',
      tab: 'web',
    },

    // backend
    {
      id: '7121',
      name: 'Java Spring',
      logo: 'assets/img/tools/spring.svg',
      link: 'https://spring.io/',
      tab: 'back-end',
      color: '#589133',
    },
    {
      id: '7122',
      name: 'Express',
      logo: 'assets/img/tools/express.png',
      link: 'https://expressjs.com/',
      tab: 'back-end',
    },
    {
      id: '7123',
      name: 'Node.js',
      logo: 'assets/img/tools/nodejs.png',
      link: 'https://nodejs.org/en/',
      tab: 'back-end',
    },
    {
      id: '7124',
      name: 'Python',
      logo: 'assets/img/tools/python.png',
      link: 'https://www.python.org/',
      tab: 'back-end',
      color: '#40c6ff',
    },
    {
      id: '7125',
      name: '.NET',
      logo: 'assets/img/tools/dotnet.svg',
      link: 'https://dotnet.microsoft.com/en-us/',
      tab: 'back-end',
    },
    
    // DB
    {
      id: '8123',
      name: 'PostgreSQL',
      logo: 'assets/img/tools/postgresql.svg',
      link: 'https://www.postgresql.org/',
      tab: 'db',
    },
    {
      id: '8124',
      name: 'MongoDB',
      logo: 'assets/img/tools/mongodb.svg',
      link: 'https://www.mongodb.com/',
      tab: 'db',
    },
    {
      id: '8125',
      name: 'Elasticsearch',
      logo: 'assets/img/tools/elasticsearch.svg',
      link: 'https://www.elastic.co/',
      tab: 'db',
    },

    // cloud
    {
      id: '6121',
      name: 'Firebase',
      logo: 'assets/img/tools/firebase.svg',
      link: 'https://firebase.google.com/',
      tab: 'cloud',
    },
    {
      id: '6122',
      name: 'GCP',
      logo: 'assets/img/tools/google-cloud.png',
      link: 'https://cloud.google.com/',
      tab: 'cloud',
    },
    {
      id: '6123',
      name: 'AWS',
      logo: 'assets/img/tools/aws.svg',
      link: 'https://aws.amazon.com/',
      tab: 'cloud',
      color: '#ECC861',
    },
    {
      id: '6124',
      name: 'Docker',
      logo: 'assets/img/tools/docker.svg',
      link: 'https://www.docker.com/',
      tab: 'cloud',
      color: '#1D63ED',
    },
    {
      id: '6125',
      name: 'Kubernetes',
      logo: 'assets/img/tools/kubernetes.svg',
      link: 'https://kubernetes.io/',
      tab: 'cloud',
    },
  ];
}
