import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { UiUtilsColor } from 'src/app/ui/utils/color.utils';

@Component({
  selector: 'app-app-item',
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.scss'],
})
export class AppItemComponent implements OnInit {
  @Input() set name(data: string) {
    if (data) {
      this._mName = data;
    }
  }

  @Input() set link(data: string) {
    if (data) {
      this._mAppUrl = data;
    }
  }

  @Input() set image(data: string) {
    if (data) {
      this._mImage = data;
    }
  }

  @Input() set color(data: string | undefined) {
    if (data) {
      this._mColor = data;
    }
  }

  _mAppUrl = '';
  _mName = '';
  _mImage?: string;
  _mColor = '#FFFFFF';

  constructor(public el: ElementRef) {}

  ngOnInit(): void {
    this.bindColor();
  }

  bindColor() {
    const element = this.el.nativeElement;
    element.style.setProperty('--app-primary', this._mColor);
    element.style.setProperty(
      '--app-primary--rgb',
      UiUtilsColor.hexToRgb(this._mColor)
    );
  }
}
