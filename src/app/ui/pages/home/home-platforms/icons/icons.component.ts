import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import {
  distinctUntilChanged,
  map,
  Observable,
  ReplaySubject,
  scan,
  startWith,
  switchMap,
  takeUntil,
  takeWhile,
} from 'rxjs';
import {
  TRANSITION_IMAGE_SCALE,
  TRANSITION_TEXT,
} from 'src/app/ui/animations/transitions/transitions.constants';
import { UiUtilsView } from 'src/app/ui/utils/views.utils';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss'],
  animations: [TRANSITION_TEXT, TRANSITION_IMAGE_SCALE],
})
export class IconsComponent implements AfterViewInit, OnDestroy {
  readonly ICONS_2: string = 'assets/img/icons/icon_set_2.png';
  readonly ICONS_2_XS = 'assets/img/icons/icon_set_2_xs.png';
  readonly ICONS_3: string = 'assets/img/icons/icon_set_3.png';
  readonly ICONS_3_XS = 'assets/img/icons/icon_set_3_xs.png';
  mOnceAnimated = false;

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  _mIcon2 = 'assets/img/icons/icon_set_2.png';
  _mIcon3 = 'assets/img/icons/icon_set_3.png';

  _mTriggerAnim? = 'false';

  _mThreshold = 0.2;

  @ViewChild('animRefView') vAnimRefView?: ElementRef<HTMLElement>;

  constructor(
    public el: ElementRef,
    private _ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    public mediaObserver: MediaObserver,
    private scroll: ScrollDispatcher,
    private viewPortRuler: ViewportRuler
  ) {
    this.mediaObserver
      .asObservable()
      .subscribe((mediaChange: MediaChange[]) => {
        if (mediaChange.length > 0) {
          if (mediaChange[0].mqAlias == 'xs') {
            this._mIcon2 = this.ICONS_2_XS;
            this._mIcon3 = this.ICONS_3_XS;
          } else {
            this._mIcon2 = this.ICONS_2;
            this._mIcon3 = this.ICONS_3;
          }
        }
      });
  }

  ngAfterViewInit(): void {
    this.setupAnimation();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  public setupAnimation() {
    if (!this.vAnimRefView) return;

    this.scroll
      .ancestorScrolled(this.vAnimRefView, 100)
      .pipe(
        // Makes sure to dispose on destroy
        takeUntil(this.destroyed$),
        startWith(0),
        map(() => {
          if (this.vAnimRefView != null) {
            const visibility = UiUtilsView.getVisibility(
              this.vAnimRefView,
              this.viewPortRuler
            );
            return visibility;
          }
          return 0;
        }),
        scan<number, boolean>(
          (acc: number | boolean, val: number) =>
            val >= this._mThreshold || (acc ? val > 0 : false)
        ),
        // Distinct the resulting triggers
        distinctUntilChanged(),
        takeWhile(trigger => {
          return !trigger || !this.mOnceAnimated;
        }, true),
        switchMap(
          trigger =>
            new Observable<number | boolean>(observer =>
              this._ngZone.run(() => observer.next(trigger))
            )
        )
      )
      .subscribe(val => {
        if (this.mOnceAnimated) {
          return;
        }

        if (val) {
          this.mOnceAnimated = true;
          this._mTriggerAnim = 'true';
          this.cdr.detectChanges();
        }
      });
  }
}
