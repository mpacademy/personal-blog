import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  distinctUntilChanged,
  map,
  Observable,
  ReplaySubject,
  scan,
  startWith,
  switchMap,
  takeUntil,
  takeWhile,
} from 'rxjs';
import { AppType } from 'src/app/types/apps_type';
import {
  TRANSITION_IMAGE_SCALE,
  TRANSITION_TEXT,
} from 'src/app/ui/animations/transitions/transitions.constants';
import { UiUtilsView } from 'src/app/ui/utils/views.utils';

@Component({
  selector: 'app-my-apps',
  templateUrl: './my-apps.component.html',
  styleUrls: ['./my-apps.component.scss'],
  animations: [TRANSITION_TEXT, TRANSITION_IMAGE_SCALE],
})
export class MyAppsComponent implements AfterViewInit, OnDestroy {
  _mApps: AppType[] = [
    {
      id: '5131',
      name: 'Online Shop',
      image: 'assets/img/apps/online_shop.png',
      link: 'https://72248490-ee76-4562-8aca-786ee9e99329-mpacademy-ad6c91d39d0be902.gitlab.io/',
      tab: 'Vue',
      caption: 'In Vue.js -',
      isFull: false,
      background: '#3FD67D',
    },
    {
      id: '5132',
      name: 'Note Board',
      image: 'assets/img/apps/note_board.png',
      link: 'http://a77617dfe1ba148879a1f6e6485b265b-278179804.us-east-1.elb.amazonaws.com/',
      tab: 'Kubernetes',
      isFull: false,
      caption: 'With Kubernetes/Angular/.NET/MongoDB -',
      background: '#F5E7B4',
    },
    {
      id: '5133',
      name: 'Nexloq',
      image: 'assets/img/apps/community.png',
      link: 'https://nexloq.com/',
      tab: 'React Native',
      caption: 'In React Native -',
      isFull: true,
      background: '#53b7f7',
    },
  ];

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  mOnceAnimated = false;

  _mTriggerAnim? = 'false';

  _mThreshold = 0.4;

  @ViewChild('animRefView') vAnimRefView?: ElementRef<HTMLElement>;

  constructor(
    public el: ElementRef,
    private _ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private scroll: ScrollDispatcher,
    private viewPortRuler: ViewportRuler
  ) {}

  ngAfterViewInit(): void {
    this.setupAnimation();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  public setupAnimation() {
    if (!this.vAnimRefView) return;
    this.scroll
      .ancestorScrolled(this.vAnimRefView, 100)
      .pipe(
        // Makes sure to dispose on destroy
        takeUntil(this.destroyed$),
        startWith(0),
        map(() => {
          if (this.vAnimRefView != null) {
            const visibility = UiUtilsView.getVisibility(
              this.vAnimRefView,
              this.viewPortRuler
            );
            return visibility;
          }
          return 0;
        }),
        scan<number, boolean>(
          (acc: number | boolean, val: number) =>
            val >= this._mThreshold || (acc ? val > 0 : false)
        ),
        // Distinct the resulting triggers
        distinctUntilChanged(),
        takeWhile(trigger => {
          return !trigger || !this.mOnceAnimated;
        }, true),
        switchMap(
          trigger =>
            new Observable<number | boolean>(observer =>
              this._ngZone.run(() => observer.next(trigger))
            )
        )
      )
      .subscribe(val => {
        if (this.mOnceAnimated) {
          return;
        }

        if (val) {
          this.mOnceAnimated = true;
          this._mTriggerAnim = 'true';
          this.cdr.detectChanges();
        }
      });
  }
}
