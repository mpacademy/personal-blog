import { Component, ElementRef, Input } from '@angular/core';
import { AppType } from 'src/app/types/apps_type';

@Component({
  selector: 'app-myapp-item',
  templateUrl: './myapp-item.component.html',
  styleUrls: ['./myapp-item.component.scss'],
})
export class MyappItemComponent {
  _mModel?: AppType;

  @Input('data') set model(data: AppType) {
    if (data) {
      this._mModel = data;
    }
  }

  constructor(public el: ElementRef) {}
}
