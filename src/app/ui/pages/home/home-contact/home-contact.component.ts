import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  distinctUntilChanged,
  map,
  Observable,
  ReplaySubject,
  scan,
  startWith,
  switchMap,
  takeUntil,
  takeWhile,
} from 'rxjs';
import { ParamPostContact } from 'src/app/api/params/contact-param';
import { ApiContactService } from 'src/app/api/repo/api-contact.service';
import {
  TRANSITION_IMAGE_SCALE,
  TRANSITION_TEXT,
} from 'src/app/ui/animations/transitions/transitions.constants';
import { DialogProgressComponent } from 'src/app/ui/common/progress/dialog-progress/dialog-progress.component';
import { DialogModelProgress } from 'src/app/ui/common/progress/model';
import { DialogSuccessComponent } from 'src/app/ui/common/success/dialog-success/dialog-success.component';
import { UiUtilsView } from 'src/app/ui/utils/views.utils';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-home-contact',
  templateUrl: './home-contact.component.html',
  styleUrls: ['./home-contact.component.scss'],
  animations: [TRANSITION_TEXT, TRANSITION_IMAGE_SCALE],
})
export class HomeContactComponent implements AfterViewInit, OnDestroy {
  mDialogSuccessRef?: MatDialogRef<DialogSuccessComponent, any>;
  mDialogProgressRef?: MatDialogRef<DialogProgressComponent, any>;

  _mFormGroup: FormGroup;

  _mInProgress = false;

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  mOnceAnimated = false;

  _mTriggerAnim? = 'false';

  _mThreshold = 0.2;

  siteKey = environment.recaptcha;

  recaptchaError = false;

  @ViewChild('animRefView') vAnimRefView?: ElementRef<HTMLElement>;

  @ViewChild('formDirective') private formDirective?: NgForm;

  constructor(
    public el: ElementRef,
    private _ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private scroll: ScrollDispatcher,
    private viewPortRuler: ViewportRuler,
    private apiContactService: ApiContactService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) {
    this._mFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.email],
      details: ['', Validators.required],
      recaptcha: ['', Validators.required],
    });
  }

  ngAfterViewInit(): void {
    this.setupAnimation();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  get name() {
    return this._mFormGroup.get('name');
  }

  get details() {
    return this._mFormGroup.get('details');
  }

  get email() {
    return this._mFormGroup.get('email');
  }

  get recaptcha() {
    return this._mFormGroup.get('recaptcha');
  }

  _onSubmit(): void {
    if (this._mFormGroup.invalid) {
      if (this.recaptcha?.invalid) {
        this.recaptchaError = true;
      }
      return;
    }
    this.recaptchaError = false;
    this._mInProgress = true;

    this.openProgress();
    const value = this._mFormGroup.getRawValue();
    this.addContact(value);
  }

  addContact(data: ParamPostContact) {
    this.apiContactService.send(data).subscribe({
      next: value => {
        this.resetForm();
        this.closeProgress();
        this.openSuccess();
      },

      error: error => {
        console.error(error);
        this.closeProgress();
      },
    });
  }

  resetForm(): void {
    this._mFormGroup.reset();
    this.formDirective?.resetForm();
  }

  public setupAnimation() {
    if (!this.vAnimRefView) return;

    this.scroll
      .ancestorScrolled(this.vAnimRefView, 100)
      .pipe(
        // Makes sure to dispose on destroy
        takeUntil(this.destroyed$),
        startWith(0),
        map(() => {
          if (this.vAnimRefView != null) {
            const visibility = UiUtilsView.getVisibility(
              this.vAnimRefView,
              this.viewPortRuler
            );
            return visibility;
          }
          return 0;
        }),
        scan<number, boolean>(
          (acc: number | boolean, val: number) =>
            val >= this._mThreshold || (acc ? val > 0 : false)
        ),
        // Distinct the resulting triggers
        distinctUntilChanged(),
        takeWhile(trigger => {
          return !trigger || !this.mOnceAnimated;
        }, true),
        switchMap(
          trigger =>
            new Observable<number | boolean>(observer =>
              this._ngZone.run(() => observer.next(trigger))
            )
        )
      )
      .subscribe(val => {
        if (this.mOnceAnimated) {
          return;
        }

        if (val) {
          this.mOnceAnimated = true;
          this._mTriggerAnim = 'true';
          this.cdr.detectChanges();
        }
      });
  }

  openProgress() {
    this._mInProgress = true;
    const alert: DialogModelProgress = new DialogModelProgress(
      'Sending...',
      '',
      undefined,
      undefined
    );

    this.mDialogProgressRef = this.dialog.open(DialogProgressComponent, {
      data: alert,
      disableClose: true,
    });
  }

  closeProgress() {
    this._mInProgress = false;
    if (this.mDialogProgressRef) this.mDialogProgressRef.close();
  }

  openSuccess() {
    this.mDialogSuccessRef = this.dialog.open(DialogSuccessComponent, {
      data: {},
      disableClose: false,
    });
    setTimeout(() => {
      this.closeSuccess();
    }, 4000);
  }

  closeSuccess() {
    if (this.mDialogSuccessRef) this.mDialogSuccessRef.close();
  }
}
