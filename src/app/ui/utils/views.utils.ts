import { ViewportRuler } from '@angular/cdk/scrolling';
import { ElementRef, Injectable } from '@angular/core';

@Injectable()
export class UiUtilsView {
  // Generates chronological unique strings
  public static getVisibility(
    elm: ElementRef<HTMLElement>,
    viewPort: ViewportRuler
  ) {
    const viewRect = viewPort.getViewportRect();
    const rect =
      elm && elm.nativeElement && elm.nativeElement.getBoundingClientRect();
    if (!rect) {
      return 0;
    }
    // Return 1.0 when the element is fully within the viewport
    if (
      rect.left > viewRect.left - 1 &&
      rect.top > viewRect.top - 1 &&
      rect.right < viewRect.right + 1 &&
      rect.bottom < viewRect.bottom + 1
    ) {
      return 1;
    }

    // Computes the intersection area otherwise
    const a = Math.round(rect.height);
    const c = Math.max(
      0,
      Math.min(rect.bottom, viewRect.bottom) - Math.max(rect.top, viewRect.top)
    );
    // Returns the amount of visible area
    return Math.round((c / a) * 10) / 10;
  }
}
