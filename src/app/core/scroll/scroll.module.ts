import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollManagerDirective } from './scroll-manager.directive';
import { ScrollSectionDirective } from './scroll-section.directive';
import { ScrollAnchorDirective } from './scroll-anchor.directive';

@NgModule({
  declarations: [
    ScrollManagerDirective,
    ScrollSectionDirective,
    ScrollAnchorDirective,
  ],
  imports: [CommonModule],
  exports: [
    ScrollManagerDirective,
    ScrollSectionDirective,
    ScrollAnchorDirective,
  ],
})
export class ScrollModule {}
