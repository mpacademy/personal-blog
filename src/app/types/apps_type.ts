export interface AppType {
  id: string;
  name: string;
  image: string;
  link: string;
  tab: string;
  caption: string;
  isFull: boolean;
  primary?: string;
  background: string;
}
