import { Inject, Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

// Services
import { ApiService } from '../common/api.service';
import { API_CONSTANTS, ApiConstants } from '../api_constants';

// Params
import { ParamPostContact } from '../params/contact-param';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiContactService {
  constructor(
    private apiService: ApiService,
    @Inject(API_CONSTANTS) public apiConstants: ApiConstants
  ) {}

  send(contact: ParamPostContact): Observable<ParamPostContact> {
    const data = {
      service_id: environment.emailServiceId,
      template_id: environment.emailTemplateId,
      user_id: environment.emailPublicKey,
      template_params: {
        user_name: contact.name,
        user_email: contact.email,
        message: contact.details,
        'g-recaptcha-response': contact.recaptcha,
      },
    };

    const urlPath = this.apiConstants.sendEmail().send;

    return this.apiService
      .post(urlPath, data, { responseType: 'text' })
      .pipe(map(() => contact));
  }
}
