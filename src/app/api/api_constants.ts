import { InjectionToken } from '@angular/core';
import { environment } from '../../environments/environment';

export const API_CONSTANTS = new InjectionToken('app.api');

export class ApiConstants {
  public static API_URL: string = environment.apiUrl;

  sendEmail() {
    return {
      send: '/send',
    };
  }
}
