export interface ParamPostContact {
  id: string;
  name: string;
  email: string;
  details: string;
  recaptcha: string;
}
