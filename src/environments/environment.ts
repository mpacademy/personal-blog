// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://api.emailjs.com/api/v1.0/email',
  emailServiceId: 'service_11qisgt',
  emailTemplateId: 'template_lhfbre2',
  emailPublicKey: 'CBM5oua6Szdx-FMV0',
  recaptcha: '6Ld3ZP0oAAAAAKVtvo8H95Tt2IWd9ULv-zL-iRpT',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
